//
//  UICardView.swift
//  Memory Game
//
//  Created by Brandon Rodriguez on 10/12/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit

class UICardView: UIView {
    
    var flipped: Bool!
    var imageHash: Int!
    var imageLayer: CALayer!
    
    func setImage(image: UIImage) {
        self.imageLayer.contents = image.CGImage;
        imageHash = image.hash;
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
        imageLayer = CALayer.init();
        imageLayer.contentsGravity = kCAGravityResizeAspect;
        self.layer.addSublayer(imageLayer);
        self.imageLayer.opacity = 0;
        self.layer.masksToBounds = false;
        self.layer.shadowOffset = CGSizeMake(2, 2)
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.25;
    }
    
    func showImage() {
        self.imageLayer.opacity = 1;
    }
    
    func hideImage() {
        self.imageLayer.opacity = 0;
    }
    
    override func isEqual(object: AnyObject?) -> Bool {
        if let object = object as? UICardView {
            return self.imageHash == object.imageHash
        } else {
            return false
        }
    }
    
    override func layoutSubviews() {
        imageLayer.frame = CGRectInset(self.bounds, 0, 0);
    }
}