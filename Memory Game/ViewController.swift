//
//  ViewController.swift
//  Memory Game
//
//  Created by Brandon Rodriguez on 10/8/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    
    private let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        playButton.layer.cornerRadius = (playButton.layer.frame.height / 2)
        playButton.layer.masksToBounds = false;
        playButton.layer.shadowOffset = CGSizeMake(2, 2)
        playButton.layer.shadowRadius = 5;
        playButton.layer.shadowOpacity = 0.25;
        
        titleLabel.layer.masksToBounds = false;
        titleLabel.layer.shadowOffset = CGSizeMake(2, 2)
        titleLabel.layer.shadowRadius = 5;
        titleLabel.layer.shadowOpacity = 0.25;
        
        self.view.addSubview(titleLabel);
        self.view.addSubview(playButton);
    }

    override func prefersStatusBarHidden() -> Bool {
        return true;
    }


}

