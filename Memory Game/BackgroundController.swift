//
//  BackgroundController.swift
//  Memory Game
//
//  Created by Brandon Rodriguez on 10/14/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit
import AVFoundation

class BackgroundController: UIViewController {
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewControllerWithIdentifier("ViewController") as! ViewController

        
        let backgroundPath: NSURL? = NSBundle.mainBundle().URLForResource("sky", withExtension: "mov");
        if let path = backgroundPath {
            
            // Video player behind blurry view
            let playerLayer: AVPlayerLayer = AVPlayerLayer(player: AVPlayer(URL: path))
            playerLayer.player?.play();
            playerLayer.videoGravity = AVLayerVideoGravityResize;
            playerLayer.needsDisplayOnBoundsChange = true;
            
            // Cool blurry effect
            let blurView: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light));
            
            // Setting stuffs
            playerLayer.frame = self.view.frame;
            self.view.layer.addSublayer(playerLayer);
            blurView.frame = self.view.frame;
            self.view.addSubview(blurView);
            
            self.addChildViewController(viewController);
            
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }

}
