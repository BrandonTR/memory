//
//  GameController.swift
//  Memory Game
//
//  Created by Brandon Rodriguez on 10/9/15.
//  Copyright © 2015 Brandon Rodriguez. All rights reserved.
//

import UIKit
import AVFoundation

class GameController: UIViewController {
    
    private let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate;
    
    @IBOutlet weak var cardContainer: UIView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var exitButton: UIButton!
    
    var firstView: UICardView!
    var pairsHidden: Int = 0
    var tapGesture: UITapGestureRecognizer!
        
    var cardImages: [UIImage?] = [UIImage(named: "casinoDice.png"), UIImage(named: "casinoHorseShoe"), UIImage(named: "casinoPartyOn"), UIImage(named: "casinoToken"), UIImage(named: "chessBlackPawn"), UIImage(named: "chessWhiteHorse"), UIImage(named: "constructionAsphalt"), UIImage(named: "constructionBricks"), UIImage(named: "constructionColumn"), UIImage(named: "constructionEarth")];
    
    var timer: NSTimer = NSTimer();
    var count: Int = 0;
    var formattedTime: String!
    
    var congratsAlert: UIAlertController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Container of our cards
        self.view.addSubview(cardContainer);
        
        // Styling on exit button and the timer
        countLabel.layer.masksToBounds = false;
        countLabel.layer.shadowOffset = CGSizeMake(2, 2)
        countLabel.layer.shadowRadius = 5;
        countLabel.layer.shadowOpacity = 0.25;
        countLabel.text = "00:00";
        self.view.addSubview(countLabel);
        
        exitButton.layer.masksToBounds = false;
        exitButton.layer.shadowOffset = CGSizeMake(2, 2)
        exitButton.layer.shadowRadius = 5;
        exitButton.layer.shadowOpacity = 0.25;
        self.view.addSubview(exitButton);
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        // Start the timer and set the cards!
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target:self, selector: Selector("incrementCount"), userInfo: nil, repeats: true);
        setCards();
    }
    
    func incrementCount () {
        // Function to increment the timer every second while setting it to the title
        count++;
        let seconds: Int = count % 60;
        let minutes: Int = (count - seconds) / 60;
        formattedTime = String(format: "%.2d:%.2d", arguments: [minutes, seconds]);
        self.countLabel.text = formattedTime;
    }
    
    func setCards () {
        // Setting the cards randomly and adding TapGestureRecognizers
        var tempArray = [UIImage?]();
        tempArray = cardImages + cardImages;
        for view in cardContainer.subviews as! [UICardView] {
            let imageIndex = Int(arc4random_uniform(UInt32(tempArray.count)))
            view.setImage(tempArray[imageIndex]!);
            tapGesture = UITapGestureRecognizer(target: self, action: "tapGestureRecognized:");
            view.addGestureRecognizer(tapGesture);
            tempArray.removeAtIndex(imageIndex);
        }
    }
    
    func tapGestureRecognized (sender: UITapGestureRecognizer) {
        
        let currentView: UICardView? = sender.view as? UICardView;
        if (currentView!.hash == firstView?.hash) {
            firstView.hideImage();
            firstView = nil;
            return;
        };
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)));
        
        // Checking your matches!
        
        if (firstView == nil) {
            
            firstView = currentView!;
            firstView.showImage()
            
        } else if firstView.isEqual(currentView) {
            
            currentView!.showImage();
            self.cardContainer.userInteractionEnabled = false;
            dispatch_after(delayTime, dispatch_get_main_queue(), { () -> Void in
                currentView?.hidden = true;
                self.firstView.hidden = true;
                self.firstView = nil;
                self.cardContainer.userInteractionEnabled = true
            })

            // Incrementing the amount of pairs hidden so we can see if you're done or not.
            pairsHidden++;
            
        } else {
            currentView?.showImage();
            self.cardContainer.userInteractionEnabled = false;
            dispatch_after(delayTime, dispatch_get_main_queue(), { () -> Void in
                currentView?.hideImage();
                self.firstView.hideImage();
                self.firstView = nil;
                self.cardContainer.userInteractionEnabled = true
            })
            
        }
    
        
        
        // Check if completed by looking at the amount of pairs hidden.
        if pairsHidden == 10 {
            timer.invalidate();
            
            // Congratulations alert formatting
            congratsAlert = UIAlertController(title: "Congratulations!", message: "You've completed all the matches in \(formattedTime)!", preferredStyle: UIAlertControllerStyle.Alert)
            congratsAlert!.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.Default, handler: {action in
                self.dismissViewControllerAnimated(true, completion: nil);
            }))
             
            self.presentViewController(congratsAlert!, animated: true, completion: nil);
        }
    }
    
    @IBAction func backToMain(sender: UIButton) {
        self.navigationController?.popToRootViewControllerAnimated(true);
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
}